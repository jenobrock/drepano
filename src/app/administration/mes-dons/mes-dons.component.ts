import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProjetAdmin } from 'src/app/models';
import firebase from 'firebase';

/**
 * @title Card with multiple sections
 */
@Component({
  selector: 'app-mes-dons',
  templateUrl: './mes-dons.component.html',
  styleUrls: ['./mes-dons.component.css'],
})
export class MesDonComponent implements OnInit {
  publications: ProjetAdmin [] = undefined;
  ngOnInit(): void {
    this.getOwnProjectsDons();
  }
  getOwnProjectsDons() {
    const add = firebase.functions().httpsCallable('own_dons_call');
    add().then((result) => {
      if (result.data.code === 200) {
        const data = result.data.data;
        this.publications.push(new ProjetAdmin(data.slug, data.id, data.title, data.description, 0));
      }
    }).catch(error => {

    });
  }

  constructor() {
  }
}
