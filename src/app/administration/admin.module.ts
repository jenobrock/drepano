import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRouting } from './admin-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AdminComponent } from './admin.component';
import { MesDonComponent } from './mes-dons/mes-dons.component';
import { MaterialModule } from '../material.module';
import { ProfileComponent } from './profile/profile.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import {MatInputModule} from '@angular/material/input';
import { PublicationCardComponent } from './publication-card/publication-card.component';
import { SouscriptionComponent } from './souscriptions/souscriptions.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AdminComponent,
  MesDonComponent, ProfileComponent, PublicationCardComponent, SouscriptionComponent],
  imports: [
    CommonModule, AdminRouting, LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
     MatListModule, MaterialModule, MaterialFileInputModule, MatInputModule, FormsModule
  ], exports: [FormsModule]
})

export class AdminModule{}

