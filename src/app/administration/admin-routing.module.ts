import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { MesDonComponent } from './mes-dons/mes-dons.component';
import { MesPublicationsComponent } from './mes-publications/mes-publications.component';
import { ProfileComponent } from './profile/profile.component';
import { MesPublicationsModule } from './mes-publications/mes-publications.module';
import { AuthGuard } from '../AuthGuard';
import { SouscriptionComponent } from './souscriptions/souscriptions.component';

const routes: Routes = [
  {
    path : '',
    component : AdminComponent,
    children : [
      {
        path : '',
        component : MesDonComponent,
      }, {
        path : 'publication',
        loadChildren: () => MesPublicationsModule,
      }, {
        path : 'profile',
        component : ProfileComponent,
      }, {
        path : 'souscriptions',
        component : SouscriptionComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRouting
{}
