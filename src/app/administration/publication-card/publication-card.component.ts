import { Component, OnInit, Input } from '@angular/core';
import { ProjetAdmin } from 'src/app/models';
@Component({
  selector: 'app-publication-card',
  templateUrl: './publication-card.component.html',
  styleUrls: ['./publication-card.component.css'],
})
export class PublicationCardComponent implements OnInit{
  @Input() publication: ProjetAdmin;
  ngOnInit(): void {}
  constructor() { }
}
