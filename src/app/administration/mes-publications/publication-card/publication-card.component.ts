import { Component, OnInit, Input } from '@angular/core';
import { ProjetAdmin } from 'src/app/models';
import firebase from 'firebase';
@Component({
  selector: 'app-publication-card',
  templateUrl: './publication-card.component.html',
  styleUrls: ['./publication-card.component.css'],
})
export class PublicationCardComponent implements OnInit{
  @Input() publication: ProjetAdmin;
  public loaded = false;
  public contribution = '';
  ngOnInit(): void {
    console.log(this.publication);
    // this.getTotalAmount();
  }
  /*getTotalAmount() {
    this.loaded = true;
    const add = firebase.functions().httpsCallable('own_dons_amount_call');
    add().then((result) => {
      if (result.data.code === 200) {
        const data = result.data.data;
        this.contribution = `Contribution : ${data}`;
      }
      this.loaded = false;
    }).catch(error => {
      this.loaded = false;
      console.log(error);
    });
  }*/
  constructor() { }
}
