import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import firebase from 'firebase';
import { ProjetAdmin } from 'src/app/models';
import { FirebaseAccess } from 'src/app/firebase.initialize';

@Component({
  selector : 'app-publication-apercu',
  templateUrl: './publication-apercu.component.html',
  styleUrls: ['../../mes-dons/mes-dons.component.css', './publication-apercu.component.css'],
})
export class PublicationApercuComponent implements OnInit{
  loaded = false;
  publications: ProjetAdmin [] = undefined;
  ngOnInit(): void {
    this.getOwnProjectsDons();
  }
  getOwnProjectsDons() {
    this.loaded = true;
    const add = this.firebaseService.getFirebase().functions().httpsCallable('own_projects_call');
    add().then((result) => {
      if (result.data.code === 200) {
        this.publications = result.data.data;
      }
      this.loaded = false;
    }).catch(error => {
      this.loaded = false;
    });
  }
  constructor(private firebaseService: FirebaseAccess) {
  }
}
