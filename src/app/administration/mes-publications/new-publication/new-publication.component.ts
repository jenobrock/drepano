import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {FormControl, Validators, NgForm} from '@angular/forms';
import { CategorieService } from 'src/app/services/categorie.service';
import { Categorie } from 'src/app/models';
import { FirebaseAccess } from 'src/app/firebase.initialize';
@Component({
  selector: 'app-new-publication',
  templateUrl: './new-publication.component.html',
  styleUrls: ['../../mes-dons/mes-dons.component.css', './new-publication.component.css'],
})
export class NewPublicationComponent implements OnInit{
  categories: Categorie[] = [];
  hasMessage = false;
  message = '';
  requesting = false;
  onSubmit(form: NgForm){
    if (!this.requesting) {
      this.createProject(form.value.title, form.value.description, form.value.cat);
      form.reset();
    }
  }
  createProject(titleValue: string, descriptionValue: string, catValue: string) {
    this.requesting = true;
    this.hasMessage = true;
    this.message = 'En cours...';
    const add = this.firebaseService.getFirebase().functions().httpsCallable('post_projet_on_call');
    add({avatar: '', description: descriptionValue, cat: catValue, intitule: titleValue}).then((result) => {
      this.message = result.data.mgs;
      this.hasMessage = true;
    }).catch(error => {
      console.log(error);
      this.hasMessage = true;
      this.message = error.error.message;
    });
  }
  ngOnInit(): void {
    this.hasMessage = true;
    this.message = 'Chargement des catégories...';
    this.categorieService.getAllCategorie().subscribe( res => {
      res.forEach(element => {
        this.categories.push(element);
      });
      this.hasMessage = false;
    }, error => {
      this.hasMessage = true;
      this.message = error.message;
    });
  }
  constructor(private firebaseService: FirebaseAccess, private categorieService: CategorieService) { }
}
