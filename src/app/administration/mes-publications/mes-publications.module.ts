import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MesPublicationsComponent } from './mes-publications.component';
import { MesPublicationsRouting } from './mes-publications-routing.module';
import { PublicationApercuComponent } from './publication-apercu/publication-apercu.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MaterialModule } from 'src/app/material.module';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatInputModule } from '@angular/material/input';
import { LayoutModule } from '@angular/cdk/layout';
import { NewPublicationComponent } from './new-publication/new-publication.component';
import { LesDonsComponent } from './les-dons/les-dons.component';
import { PublicationCardComponent } from './publication-card/publication-card.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MesPublicationsComponent, LesDonsComponent, PublicationApercuComponent, NewPublicationComponent, PublicationCardComponent],
  imports: [
    CommonModule, MesPublicationsRouting, LayoutModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
    MatListModule, MaterialModule, MaterialFileInputModule, MatInputModule, FormsModule
  ]
})

export class MesPublicationsModule{}

