import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MesPublicationsComponent } from './mes-publications.component';
import { PublicationApercuComponent } from './publication-apercu/publication-apercu.component';
import { NewPublicationComponent } from './new-publication/new-publication.component';
import { LesDonsComponent } from './les-dons/les-dons.component';
import { AuthGuard } from '../../AuthGuard';
const routes: Routes = [
  {
    path : '',
    component : MesPublicationsComponent,
    children : [
      {
        path : '',
        component : PublicationApercuComponent,
      }, {
        path : 'dons',
        component : LesDonsComponent,
      }, {
        path : 'new',
        component : NewPublicationComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MesPublicationsRouting
{}
