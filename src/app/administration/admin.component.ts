import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { AuthServiceService } from '../services/auth-service.service';
import { Userconnected } from '../models';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector : 'app-admin',
  templateUrl : './admin.component.html',
  styleUrls : ['./admin.component.css']
})

export class AdminComponent implements OnInit{
  public userConnected: Userconnected;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
    );

  constructor(private breakpointObserver: BreakpointObserver, private authenticationService: AuthServiceService
    ,         private cookieService: CookieService) {}
  ngOnInit(): void {
    this.userConnected = this.authenticationService.getCurrentUser();
  }

}
