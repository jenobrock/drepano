import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {FormControl, Validators} from '@angular/forms';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { CookieService } from 'ngx-cookie-service';
import { Userconnected } from 'src/app/models';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['../mes-dons/mes-dons.component.css', './profile.component.css'],
})
export class ProfileComponent implements OnInit{
  public userConnected: Userconnected;
  usernameFormControl;
  pwdFormControl;
  nomUserFormControl;
  col: number;
  row: number;
  ngOnInit(): void {
    if (this.breakpointObserver.isMatched([Breakpoints.Medium, Breakpoints.Large])) {
      this.col = 12 ;
      this.row = 1;
    }else{
      this.col = 12;
      this.row = 1;
    }
  }
  constructor(private breakpointObserver: BreakpointObserver, private authenticationService: AuthServiceService
    ,         private cookieService: CookieService) {
    this.userConnected = this.authenticationService.getCurrentUser();
    this.usernameFormControl = this.userConnected.email;
    this.nomUserFormControl = this.userConnected.displayName;
    this.pwdFormControl = this.userConnected.email;
  }
}
