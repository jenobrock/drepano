import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import { Souscription } from 'src/app/models';

@Component({
  selector: 'app-souscription',
  templateUrl: './souscriptions.component.html',
  styleUrls: ['../mes-dons/mes-dons.component.css', './souscriptions.component.css'],
})
export class SouscriptionComponent implements OnInit{
  datas: Souscription[] = [];
  requesting = false;
  displayedColumns: string[] = ['displayName', 'dateCreation', 'message', 'object', 'email'];
  ngOnInit(): void {
    this.requesting = true;
    const add = firebase.functions().httpsCallable('souscriptions_call');
    add().then((result) => {
      console.log(result);
      if (result.data.code === 200) {
        this.datas = result.data.data;
      }
      this.requesting = false;
    }).catch(error => {
      this.requesting = false;
      console.log(error);
    });
  }
}
