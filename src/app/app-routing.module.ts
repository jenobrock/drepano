import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { AdminModule } from './administration/admin.module';
import { PublicModule } from './public/public.module';
import { NotFoundComponent } from './notfound/notfound.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './AuthGuard';
const routerOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
};
const routes: Routes = [

    {
    path: '',
    loadChildren: () => PublicModule,
  }/*, {
    path: 'admin',
    loadChildren: () => AdminModule,
    canActivateChild: [AuthGuard],
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuard],
  }*/,
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
