export class Userconnected {
  constructor(public displayName: string, public email: string, public profile: {libelle: string, group: boolean, admin: boolean}) {}
}


export class TopCategorie {
  constructor(public category: string, public id: string, public top: string) {

  }
}
export class Categorie {
  constructor(public category: string, public id: string) {}
}

export class Souscription{
  constructor(public displayName: string, public dateCreation: string, public message: string,
              public object: string, public email: string){

  }
}

export class PublicationRecentes {
  avatar: string;
  creation: string;
  title: string;
  creator: string;
  slug: string;
  id: string;
  /*constructor(public avatar: string, public creation: string, public title: string, creator: string, slug: string, public id: string) {

  }*/
}

export class PublicationPaginateData {
  constructor(public data: PublicationRecentes[], public meta: {next, prev}) {
  }
}
export class ClusterDetail{
  constructor(public avatar: string, public id: string, public raison: string, public description: string,
              public phones: string, public residences: string,
              public mail: string, public projects: {}){}
}

export class ClusterShort{
  constructor(public avatar: string, public raison: string, public slug: string){}
}


export class Publication {
  constructor(public avatar: string, public don: number, public title: string, public slug: string, public id: string) {

  }
}

export class DetailPublication {
  constructor(public avatar: string, public id: string, public creation: string
    ,         public title: string, public slug: string, public creator: string, public dons: number
    ,         public description: string, public comments: number) {

  }
}

export class ProjetAdmin{
  constructor(public slug: string, public id: string, public title: string, public description: string, public dons: number){
  }
}



export class Commentaire {
  constructor(public id: string, public content: string, public creation: string, public user: string,
              public comments: number, public subcomments: Commentaire[], public respond: boolean, public showSub: boolean) {
  }
}

