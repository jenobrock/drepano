import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector : 'app-logout',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit{
  constructor(private authenticationService: AuthServiceService, private router: Router) {

  }
  ngOnInit(): void {
    this.authenticationService.signOutUser();
    setTimeout(() => {
      this.router.navigate(['']);
    }, 1000)
    //this.router.navigate(['']);
    //window.location.assign('')
  }
}
