import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { SubscribeService } from 'src/app/services/subscribe.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  loader = false;
  public hasMessage = false;
  public message;
  constructor(private scrollTopService: ScrollTopService, private subscribeService: SubscribeService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }

  onSubscribe(form: NgForm) {
    const email = form.value.email;
    if (email.length !== 0) {
      this.loader = true;
      this.subscribeService.getSubscribe(form.value.message, form.value.names, email, form.value.object, true).subscribe((result) => {
        this.loader = false;
        this.hasMessage = true;
        this.message = result.message;
        form.reset();
      },
      (err) => {
        this.loader = false;
        this.hasMessage = true;
        this.message = err.error.message;
        console.log(err);
        form.reset();
      });
    }
  }
}
