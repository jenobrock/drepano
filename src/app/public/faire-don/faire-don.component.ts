import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { ActivatedRoute } from '@angular/router';
import { Userconnected, DetailPublication } from 'src/app/models';
import { randomImage } from 'src/app/utils/utils';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-faire-don',
  templateUrl: './faire-don.component.html',
  styleUrls: ['./faire-don.component.css']
})
export class FaireDonComponent implements OnInit {
  slug: string = undefined;
  userConnected: Userconnected = undefined;
  public imageAlt;
  projet: DetailPublication;
  projetTitle;
  public image;
  constructor( private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private scrollTopService: ScrollTopService,
               private authenticationService: AuthServiceService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
    this.userConnected = this.authenticationService.getCurrentUser();
    this.activatedRoute.params.subscribe(params => {
      this.slug = params.slug;
      if (this.slug !== undefined) {
        this.projetTitle = 'Chargement...';
        this.getPublicationDetail(this.slug);
      }
    });
  }
  getPublicationDetail(slug: string) {
    this.httpClient.get<DetailPublication>('https://us-central1-drepano-test.cloudfunctions.net/projet_detail?p=' + slug)
      .subscribe((res) => {
        this.projet = res;
        this.projetTitle = this.projet.title.toUpperCase();
        if (this.projet) {
          this.imageAlt = `${this.projet.title} image`;
          if (this.projet.avatar === '') {
            this.image = randomImage();
          } else {
            this.image = this.projet.avatar;
          }
        }
      });
  }

}
