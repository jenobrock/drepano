import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { PublicationRecentes, Categorie, PublicationPaginateData } from 'src/app/models';
import { CategorieService  } from 'src/app/services/categorie.service';

import { RouterLinkActive, ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
  publications: PublicationRecentes [] = [];
  categories: Categorie[] = [];
  loaded: boolean;
  nextUrl: string;
  prevUrl: string;
  constructor(private httpClient: HttpClient, private route: Router, private categorieService: CategorieService) { }

  ngOnInit(): void {
    this.categorieService.getAllCategorie().subscribe( res => {
      res.forEach(element => {
        this.categories.push(element);
      });
    });
  }
  onSubmit(form: NgForm ){
    this.getPublicationRecent(form.value.qword, form.value.cat, form.value.re);
  }

  next(): void{
     this.researchRequest(this.nextUrl);
  }

  prev(): void{
     this.researchRequest(this.prevUrl);
  }
  getPublicationRecent(q: string, cat: string, re: string ){
    let query = '';
    if (q !== undefined && q !== '') {
      query = `q=${q}`;
    }
    if (cat !== undefined && cat !== '') {
      query = `${query}&cat=${cat}`;
    }
    if (re === 'top') {
      query = `${query}&top`;
    }
    if (query !== '') {
      query = `?${query}`;
    }
    this.researchRequest(`https://us-central1-drepano-test.cloudfunctions.net/publication_search${query}`);
   }

   researchRequest(query){
     this.loaded = true;
     this.publications = [];
     this.httpClient.get<PublicationPaginateData>(query).subscribe(res => {
       this.loaded = false;
       // tslint:disable-next-line:no-string-literal
       res.data.forEach(element => {
        this.publications.push(element);
      });
      // tslint:disable-next-line:align
      // tslint:disable-next-line:no-string-literal
       this.nextUrl = res.meta.next;
       this.prevUrl = res.meta.prev;
    });
   }

   go(i: any){
      console.log(i);
      this.route.navigate(['publication', 'detail', i]);
   }
}
