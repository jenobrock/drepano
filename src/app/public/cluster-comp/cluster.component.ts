import { Component, Input, OnInit } from '@angular/core';
import { ClusterShort } from 'src/app/models';
import { randomImage } from 'src/app/utils/utils';

@Component({
  selector : 'app-cluster-comp',
  templateUrl: './cluster.component.html',
  styleUrls: ['./cluster.component.css']
})
export class ClusterCompComComponent implements OnInit{
  @Input() cluster: ClusterShort;
  public imageAlt;
  public image;

  ngOnInit(): void {
    this.imageAlt = `${this.cluster.raison} image`;
    if (this.cluster.avatar === '' || this.cluster.avatar === undefined) {
      this.image = randomImage();
    }else{
      this.image = this.cluster.avatar;
    }
  }
}
