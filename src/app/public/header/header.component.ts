import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../services/auth-service.service';
import { Userconnected } from '../../models';
import firebase from 'firebase';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public userConnected: Userconnected;
  constructor(private authenticationService: AuthServiceService, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.userConnected = this.authenticationService.getCurrentUser();
  }
}
