import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DetailPublication, Userconnected, Commentaire } from 'src/app/models';
import * as firebase from 'firebase';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { NgForm } from '@angular/forms';
import { AuthServiceService } from '../../services/auth-service.service';
import { element } from 'protractor';
import { randomImage } from 'src/app/utils/utils';

@Component({
  selector: 'app-detail-publication',
  templateUrl: './detail-publication.component.html',
  styleUrls: ['./detail-publication.component.css']
})
export class DetailPublicationComponent implements OnInit {
  loaded = false;
  commentloaded = false;
  projet: DetailPublication;
  commentaires: Commentaire[] = undefined;
  projetTitle = 'Chargement...';
  slug: string = undefined;
  userConnected: Userconnected;
  public imageAlt;
  public image;
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private scrollTopService: ScrollTopService,
              private authenticationService: AuthServiceService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
    this.userConnected = this.authenticationService.getCurrentUser();
    this.activatedRoute.params.subscribe(params => {
      this.slug = params.slug;
      if (this.slug !== undefined) {
        this.getPublicationDetail(this.slug);
      }
    });
  }

  onSubmit(form: NgForm) {
    if (form.value.content !== '') {
      this.createComment(this.projet.id, undefined, form.value.content, form);
      form.controls.content.setValue('');
    }
  }

  toggleRespond(comment: Commentaire) {
    comment.respond = !comment.respond;
    comment.showSub = false;
  }

  toggleSubComment(comment: Commentaire) {
    comment.showSub = !comment.showSub;
    comment.respond = false;
  }

  onSubComment(form: NgForm, comment: Commentaire) {
    if (form.value.content !== '' && form.controls.content.disabled === false) {
      this.createComment(undefined, comment, form.value.content, form);
      comment.showSub = true;
    }
  }

  getPublicationDetail(slug: string) {
    this.httpClient.get<DetailPublication>('https://us-central1-drepano-test.cloudfunctions.net/projet_detail?p=' + slug)
      .subscribe((res) => {
        this.projet = res;
        this.projetTitle = this.projet.title;
        if (this.projet) {
          this.imageAlt = `${this.projet.title} image`;
          if (this.projet.avatar === '') {
            this.image = randomImage();
          } else {
            this.image = this.projet.avatar;
          }
          this.getCommentaires(this.projet.id);
        }
      }, (error) => {
      });
  }
  getCommentaires(id: string) {
    this.commentloaded = true;
    this.httpClient.get<Commentaire[]>('https://us-central1-drepano-test.cloudfunctions.net/get_comments?p=' + id).pipe()
      .subscribe((res) => {
        this.commentaires = res;
        this.commentaires.forEach(comment => {
          this.getSubCommentaires(comment);
        });
        this.commentloaded = false;
      });
  }

  getSubCommentaires(comment: Commentaire) {
    this.httpClient.get<Commentaire[]>('https://us-central1-drepano-test.cloudfunctions.net/get_comments?c=' + comment.id).pipe()
      .subscribe((res) => {
        comment.subcomments = res;
      });
  }

  createComment(projectId, comment: Commentaire, contentComment, form: NgForm) {
    const add = firebase.functions().httpsCallable('comment_on_call');
    if (projectId !== undefined) {
      this.loaded = true;
      form.controls.content.disable();
      add({ project: projectId, content: contentComment }).then((result) => {
        form.controls.content.enable();
        this.loaded = false;
        if (result.data.code === 200) {
          const data = result.data.data;
          this.commentaires.push(new Commentaire(data.id, data.content, data.creation, data.user, data.comments, undefined, false, false));
          this.projet.comments = this.projet.comments + 1;
        }
      }).catch((err) => {
        form.controls.content.enable();
        this.loaded = false;
      });
    } else {
      form.controls.content.disable();
      add({ comment: comment.id, content: contentComment }).then((result) => {
        form.controls.content.enable();
        form.controls.content.setValue('');
        if (result.data.code === 200) {
          const data = result.data.data;
          if (comment.subcomments === undefined) {
            comment.subcomments = [];
          }
          comment.subcomments.push(new Commentaire(data.id, data.content, data.creation,
            data.user, data.comments, undefined, false, false));
          comment.comments = comment.comments + 1;
          this.projet.comments = this.projet.comments + 1;
        }
      }).catch((err) => {
        console.log(err);
        form.controls.content.enable();
      });
    }
  }
}
