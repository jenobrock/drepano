import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { ClusterService } from 'src/app/services/cluster.service';
import { ClusterDetail, ClusterShort } from 'src/app/models';
import { randomImage } from 'src/app/utils/utils';

@Component({
  selector: 'app-organisations',
  templateUrl: './organisations.component.html',
  styleUrls: ['./organisations.component.css']
})
export class OrganisationsComponent implements OnInit {
  private slug;
  public image;
  public loaded = false;
  public clusterDetail: ClusterDetail;
  public clusters: ClusterShort[] = [];
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient, private scrollTopService: ScrollTopService,
              private clusterService: ClusterService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
    this.getClusters();
    this.activatedRoute.params.subscribe(params => {
      this.slug = params.slug;
      if (this.slug !== undefined) {
        this.getClusterDetail(this.slug);
      }
    });

  }

  getClusterDetail(slug: string){
    this.loaded = true;
    this.clusterService.getClusterDetail(slug).pipe().subscribe((res) => {
      this.clusterDetail = res;
      if (this.clusterDetail.avatar === '' || !this.clusterDetail.avatar) {
        this.image = randomImage();
      }else{
        this.image = this.clusterDetail.avatar;
      }
      this.loaded = false;
    });
  }

  getClusters(){
    this.loaded = true;
    this.clusterService.getClusterShorts().pipe().subscribe((res) => {
      this.clusters = res;
      this.loaded = false;
    });
  }

}
