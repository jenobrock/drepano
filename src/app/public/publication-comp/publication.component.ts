import { Component, Input, OnInit } from '@angular/core';
import { PublicationRecentes } from 'src/app/models';
import { randomImage } from 'src/app/utils/utils';

@Component({
  selector : 'app-publication-comp',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComComponent implements OnInit{
  @Input() publication: PublicationRecentes;
  public imageAlt;
  public image;

  ngOnInit(): void {
    this.imageAlt = `${this.publication.title} image`;
    if (this.publication.avatar === '' || this.publication.avatar === undefined) {
      this.image = randomImage();
    }else{
      this.image = this.publication.avatar;
    }
  }
}
