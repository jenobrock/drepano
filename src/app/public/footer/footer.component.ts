import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SubscribeService } from 'src/app/services/subscribe.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  hasMessage = false;
  message = '';
  loader = false;
  
  constructor(private subscribeService: SubscribeService) { }

  ngOnInit(): void {
  }

  onSubscribe(form: NgForm){
    const email: string = form.value.email;
    if (email.length !== 0) {
      this.loader = true;
      this.subscribeService.getSubscribe('', '', form.value.email, '', false).subscribe((result) => {
        this.hasMessage = true;
        this.message = result.message;
        this.loader = false;
        form.reset();
      },(error) => {
        this.hasMessage = true;
        this.message = error.error.message;
      });
    }
  }
  onSubscribeToMailchimp(form: NgForm){
    const email: string = form.value.email;
    if (email.length !== 0) {
      this.loader = true;
      this.subscribeService.subscribeToMailChimp(form).pipe().subscribe(() => {
        this.loader = false;
        this.hasMessage = true;
        this.message = "Souscription avec succès. Veuillez confirmer votre souscription en validant le mail réçu"
      });
    }
  }
}
