import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from '../services/scrolltop.service';

@Component({
  selector : 'app-public',
  templateUrl : './public.component.html',
  styleUrls : ['./public.component.css']
})

export class PublicComponent implements OnInit{
  constructor(private scrollTopService: ScrollTopService) {

  }
  ngOnInit() {
    this.scrollTopService.setScrollTop();
  }
}
