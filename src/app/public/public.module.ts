import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public.component';
import { PublicRouting } from './public-routing.module';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { PublicationComponent } from './publication/publication.component';
import { OrganisationsComponent } from './organisations/organisations.component';
import { DetailPublicationComponent } from './detail-publication/detail-publication.component';
import { ContactComponent } from './contact/contact.component';
import { FaireDonComponent } from './faire-don/faire-don.component';
import { AccountComponent } from './account/account.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './acceuil/slider/slider.component';
import { CategorieComponent } from './acceuil/categorie/categorie.component';
import { CategoriePopulaireComponent } from './acceuil/categorie-populaire/categorie-populaire.component';
import { PublicationsComponent } from './acceuil/publications/publications.component';
import { PublicationsRecentesComponent } from './acceuil/publications-recentes/publications-recentes.component';
import { ClusterComponent } from './acceuil/cluster/cluster.component';
import { BannerComponent } from './banner/banner.component';
import { BodyComponent } from './publication/body/body.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { PublicationComComponent } from './publication-comp/publication.component';
import { PubliciteComponent } from './publicites/publicite.component';
import { ClusterCompComComponent } from './cluster-comp/cluster.component';
import { AcceuilNewComponent } from './acceuil_new/acceuil.component';
import { ActualiteComponent } from './acceuil_new/actualites/actualite.component';
import { DrepanoComponent } from './acceuil_new/drepano/drepano.component';


@NgModule({
  declarations: [
    PublicComponent,
    AcceuilComponent,
    PublicationComponent,
    OrganisationsComponent,
    DetailPublicationComponent,
    ContactComponent,
    FaireDonComponent,
    AccountComponent,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    CategorieComponent,
    CategoriePopulaireComponent,
    PublicationsComponent,
    PublicationsRecentesComponent,
    ClusterComponent,
    BannerComponent,
    PublicationComComponent,
    ClusterCompComComponent,
    PubliciteComponent,
    BodyComponent, AcceuilNewComponent, ActualiteComponent, DrepanoComponent],
  imports: [
    CommonModule, PublicRouting, FormsModule,
    HttpClientModule, HttpClientJsonpModule
  ]
})

export class PublicModule{}

