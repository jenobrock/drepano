import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { AuthServiceService } from 'src/app/services/auth-service.service';


@Component({
  selector: 'app-drepano',
  templateUrl: './drepano.component.html',
  styleUrls: ['./drepano.component.css']
})
export class DrepanoComponent implements OnInit {

  constructor(private auths: AuthServiceService, private scrollTopService: ScrollTopService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }

}
