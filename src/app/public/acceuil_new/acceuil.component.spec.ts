import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceuilNewComponent } from './acceuil.component';

describe('AcceuilComponent', () => {
  let component: AcceuilNewComponent;
  let fixture: ComponentFixture<AcceuilNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceuilNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceuilNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
