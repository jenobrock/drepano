import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../services/auth-service.service';
import { ScrollTopService } from 'src/app/services/scrolltop.service';


@Component({
  selector: 'app-acceuil-new',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilNewComponent implements OnInit {

  constructor(private auths: AuthServiceService, private scrollTopService: ScrollTopService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }

}
