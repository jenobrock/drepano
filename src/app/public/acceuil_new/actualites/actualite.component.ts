import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { AuthServiceService } from 'src/app/services/auth-service.service';


@Component({
  selector: 'app-actualite',
  templateUrl: './actualite.component.html',
  styleUrls: ['./actualite.component.css']
})
export class ActualiteComponent implements OnInit {

  constructor(private auths: AuthServiceService, private scrollTopService: ScrollTopService) { }

  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }

}
