import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PublicationService } from 'src/app/services/publication.service';
import { Publication } from 'src/app/models';
import { PublicationRecentes } from 'src/app/models';
@Component({
  selector: 'app-publications-recentes',
  templateUrl: './publications-recentes.component.html',
  styleUrls: ['./publications-recentes.component.css']
})
export class PublicationsRecentesComponent implements OnInit {
  publications: PublicationRecentes [] = [];
  loaded = true;
  constructor(private http: HttpClient, private publication: PublicationService) { }

  ngOnInit(): void {
    this.publication.getPublicationRecent().subscribe((res) => {
      res.forEach(element => {
        this.publications.push(element);
      });
      this.loaded = false;
      });
  }

}
