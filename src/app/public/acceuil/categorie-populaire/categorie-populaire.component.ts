import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CategorieService } from 'src/app/services/categorie.service';
import { TopCategorie } from 'src/app/models';

@Component({
  selector: 'app-categorie-populaire',
  templateUrl: './categorie-populaire.component.html',
  styleUrls: ['./categorie-populaire.component.css']
})
export class CategoriePopulaireComponent implements OnInit {
  categories: TopCategorie [] = []
  loaded: boolean = true
  constructor( private http: HttpClient, private categorie: CategorieService) { }

  ngOnInit(): void {
    this.categorie.getCategorie().subscribe((res)=>{

    res.forEach(element => {
      // tslint:disable-next-line: no-unused-expression
      this.categories.push(element);
   //   console.log(element)

    });
    this.loaded= false
    })
    //.log(this.categories[0])
  }

}
