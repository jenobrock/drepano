import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PublicationService } from 'src/app/services/publication.service';
import { Publication } from 'src/app/models';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css']
})
export class PublicationsComponent implements OnInit {
  publications: Publication[] = [];
  loaded = true;

  constructor(private http: HttpClient, private publication: PublicationService) { }

  ngOnInit(): void {
    this.publication.getPublication().subscribe((res) => {
      res.forEach(element => {
        this.publications.push(element);
      });
      this.loaded = false;
    });
  }
}
