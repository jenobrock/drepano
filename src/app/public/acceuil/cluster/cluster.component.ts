import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ScrollTopService } from 'src/app/services/scrolltop.service';
import { ActivatedRoute } from '@angular/router';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { ClusterService } from 'src/app/services/cluster.service';
import { ClusterDetail, ClusterShort } from 'src/app/models';


@Component({
  selector: 'app-cluster',
  templateUrl: './cluster.component.html',
  styleUrls: ['./cluster.component.css']
})
export class ClusterComponent implements OnInit {
  public loaded = false;
  public clusters: ClusterShort[] = undefined;
  constructor(private clusterService: ClusterService) { }

  ngOnInit(): void {
    this.getClusters();
  }
  getClusters(){
    this.loaded = true;
    this.clusterService.getClusterShorts().pipe().subscribe((res) => {
      this.clusters = res;
      this.loaded = false;
    });
  }
}
