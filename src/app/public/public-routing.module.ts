import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { OrganisationsComponent } from './organisations/organisations.component';
import { ContactComponent } from './contact/contact.component';
import { DetailPublicationComponent } from './detail-publication/detail-publication.component';
import { PublicationComponent } from './publication/publication.component';
import { AccountComponent } from './account/account.component';
import { PublicComponent } from './public.component';
import { FaireDonComponent } from './faire-don/faire-don.component';
import { AcceuilNewComponent } from './acceuil_new/acceuil.component';


const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children : [
      {
        path: '', component: AcceuilNewComponent
      }/*,
      {
        path: 'dons', component: FaireDonComponent
      },
      {
        path: 'dons/:slug', component: FaireDonComponent
      }*/,
      {
        path: 'organisations', component: OrganisationsComponent
      },
      {
        path: 'organisations/:slug', component: OrganisationsComponent
      }
      ,
      {
        path: 'contact', component: ContactComponent
      }/*, {
        path: 'publications', component: PublicationComponent
      },
      {
        path: 'publications/:slug', component: DetailPublicationComponent
      }*/,
      {
        path: 'authentification', component: AccountComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PublicRouting { }
