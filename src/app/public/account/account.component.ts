import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthServiceService } from '../../services/auth-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';
import { ScrollTopService } from 'src/app/services/scrolltop.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  public loaded = false;
  public hasError = false;
  public error = '';
  HTTP_OPTIONS = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  creation = false;
  constructor(
    private scrollTopService: ScrollTopService,
    private router: Router,
    private authservice: AuthServiceService,
    private http: HttpClient
  ) {}

  toggleForm(){
    this.creation = !this.creation;

  }
  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }
  onSubmit(form: NgForm) {
    this.loaded = true;
    this.hasError = false;
    const url = 'https://us-central1-drepano-test.cloudfunctions.net/create_user_actif';
    this.http.post<{message: string}>(url, { email: form.value.email, pwd: form.value.password, displayName: form.value.username, }
      ).subscribe(
        (res) => {
          console.log(res);
          this.hasError = true;
          this.loaded = false;
          this.error = res.message;
        },
        (err) => {
          this.loaded = false;
          this.hasError = true;
          this.error = err.error.message;
        }
      );
  }

  onSubmitGroupe(formGroupe: NgForm){
    this.loaded = true;
    this.hasError = false;
    const rccmValue = formGroupe.value.rccm;
    const raisonValue = formGroupe.value.raison;
    const idnatValue = formGroupe.value.idnat;
    const sloganValue = formGroupe.value.slogan;
    const descriptionValue = formGroupe.value.description;
    const residencesValue = formGroupe.value.residence;
    const phonesValue = formGroupe.value.phones;
    const contactValue = formGroupe.value.contact;
    const formResponseValue: {} = { rccm: rccmValue,
      raison_sociale: raisonValue,
      id_nat: idnatValue,
      slogan: sloganValue,
      description: descriptionValue
    };
    if (rccmValue.length !== 0 && raisonValue.length !== 0 && idnatValue.length !== 0
      && sloganValue.length !== 0 && descriptionValue.length !== 0
      && residencesValue.length !== 0 && phonesValue.length !== 0
      && raisonValue !== 'N/A' && raisonValue !== 'NA') {
        const url = 'https://us-central1-drepano-test.cloudfunctions.net/create_user_group';
        this.http.post<{message: string}>(url, { email: formGroupe.value.email, pwd: formGroupe.value.password,
           form_response: formResponseValue, contact: contactValue, phones: phonesValue, residence: residencesValue} ,
        this.HTTP_OPTIONS
      ).subscribe(
        (res) => {
          this.hasError = true;
          this.loaded = false;
          this.error = res.message;
        },
        (err) => {
          this.loaded = false;
          this.hasError = true;
          this.error = err.error.message;
          console.log(err);
        }
      );
    }else{
      this.hasError = true;
      this.error = 'Veuillez remplir tous les champs, pour les informations que vous n\'avez pas, veuillez entrer N/A ou NA sauf pour la raison sociale';
      this.loaded = false;
    }
  }

  login(form: NgForm){
    this.loaded = true;
    this.hasError = false;
    const user: string = form.value.username;
    const pass: string = form.value.password;

    this.authservice.signUser(user, pass).then((res) => {
      this.hasError = false;
      this.loaded = false;
      setTimeout(() => {
        this.router.navigate(['']);
      }, 1000);
    }).catch(err => {
      this.loaded = false;
      this.hasError = true;
      this.error = err.error.message;
    });
  }
}

