import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, CanActivateChild } from '@angular/router';
import { AuthServiceService } from './services/auth-service.service';
import { FirebaseAccess } from './firebase.initialize';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanLoad, CanActivateChild {
  // tslint:disable-next-line:max-line-length
  canLoad(route: import('@angular/router').Route, segments: import('@angular/router').UrlSegment[]): boolean | import('rxjs').Observable<boolean> | Promise<boolean> {
    return this.check();
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import('@angular/router').UrlTree | import('rxjs').Observable<boolean | import('@angular/router').UrlTree> | Promise<boolean | import('@angular/router').UrlTree> {
      return this.check();
  }
    constructor(
        private router: Router,
        private authenticationService: AuthServiceService,
        private firebaseService: FirebaseAccess
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.check();
    }

    check(): boolean{
      const currentUser = this.authenticationService.getCurrentUser();
      if (currentUser) {
            return true;
      }
      this.router.navigate(['/']);
      return false;
    }
}
