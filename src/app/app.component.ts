import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from './services/scrolltop.service';
import { FirebaseAccess } from './firebase.initialize';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  ngOnInit(): void {
    this.scrollTopService.setScrollTop();
  }
  constructor(private scrollTopService: ScrollTopService, firebaseInit: FirebaseAccess){
    firebaseInit.initialize();
  }
}
