const imagesList = ['assets/img/svg_icon/1.svg', 'assets/img/svg_icon/2.svg',
  'assets/img/svg_icon/3.svg',
  'assets/img/svg_icon/4.svg',
  'assets/img/svg_icon/5.svg',
  'assets/img/svg_icon/6.svg'];
export function randomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomImage(){
  return imagesList[randomInt(0, 5)] || '';
}
