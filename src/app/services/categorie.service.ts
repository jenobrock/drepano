import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TopCategorie, Categorie } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(private httpClient: HttpClient) {}
  getCategorie(){
   return this.httpClient.get<TopCategorie[]>('https://us-central1-drepano-test.cloudfunctions.net/get_top_categories').pipe();
  }
  getAllCategorie(){
    return this.httpClient.get<Categorie[]>('https://us-central1-drepano-test.cloudfunctions.net/get_categories').pipe();
  }
}
