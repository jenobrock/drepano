

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClusterDetail, ClusterShort } from '../models';
@Injectable({
  providedIn: 'root'
})
export class ClusterService {
  constructor(private httpClient: HttpClient) { }
  getClusterDetail(param: string) {
    return this.httpClient.get<ClusterDetail>(`https://us-central1-drepano-test.cloudfunctions.net/cluster_detail?c=${param}`);
  }

  getClusterShorts() {
    return this.httpClient.get<ClusterShort[]>(`https://us-central1-drepano-test.cloudfunctions.net/get_cluster_short`);
  }
}
