import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class SubscribeService {
  mailChimpEndpoint = 'https://gmail.us18.list-manage.com/subscribe/post?u=fc9b79e2c60f43d61f957d55f&amp;id=b74c69b08e';
  constructor(private httpClient: HttpClient) {}
  getSubscribe(messageValue: string, names: string, mail: string, objectValue: string, isMessageValue: boolean){
    const url = 'https://us-central1-drepano-test.cloudfunctions.net/subscribe';
    return this.httpClient.post<{message: string}>(url, {email : mail, object : objectValue, displayName: names, message: messageValue
    , isMessage : isMessageValue});
  }
  subscribeToMailChimp(form : NgForm){
    const params = new HttpParams()
      .set('FNAME', form.value.prenom)
      .set('LNAME', form.value.nom)
      .set('EMAIL', form.value.email)
      //.set('group[24413][128]', 'true')
      .set('b_fc9b79e2c60f43d61f957d55f_b74c69b08e', '');
    const mailChimpUrl = `${this.mailChimpEndpoint}&${params.toString()}`;
    return this.httpClient.jsonp(mailChimpUrl,'c');
  }
}
