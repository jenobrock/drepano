import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import firebase from 'firebase';
import { Userconnected } from 'src/app/models';
import $ from 'jquery';
import {CookieService} from 'ngx-cookie-service';
import { FirebaseAccess } from '../firebase.initialize';



@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  public currentUser: Userconnected;
  constructor(private httpClient: HttpClient, private cookieService: CookieService, private firebaseService: FirebaseAccess) {
    this.currentUser = undefined;
    this.firebaseService.getFirebase().auth().onAuthStateChanged(user => {
      if (user && user !== undefined && user !== null) {
        this.currentUser = new Userconnected(user.displayName, user.email, {libelle: '', group: false, admin: false});
        /*this.cookieService.deleteAll();
        this.cookieService.set('currentUser', JSON.stringify(this.currentUser));*/
        localStorage.clear();
        localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        let data;
        const add = this.firebaseService.getFirebase().functions().httpsCallable('get_profile_call');
        add().then((result) => {
          if (result.data.code === 200) {
            data = result.data.data;
            this.currentUser = new Userconnected(user.displayName, user.email, {libelle: data.libelle || '',
             group: data.group || false, admin: data.admin || false});

          }else{
            this.currentUser = new Userconnected(user.displayName, user.email, {libelle: '', group: false, admin: false});
          }
          /*this.cookieService.deleteAll();
          this.cookieService.set('currentUser', JSON.stringify(this.currentUser));*/
          localStorage.clear();
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
        }).catch(error => {
          console.log(error);
        });
      } else {

        /*this.cookieService.deleteAll();*/
        localStorage.clear();
        this.currentUser = undefined;
      }
    });
  }

  public getCurrentUser(): Userconnected {
    if (!localStorage.getItem('currentUser') || localStorage.getItem('currentUser') === null) {
      return undefined;
    } else {
      return JSON.parse(localStorage.getItem('currentUser'));
    }
    /*if (this.cookieService.get('currentUser').length === 0 || this.cookieService.get('currentUser') === null) {
      return undefined;
    } else {
      return JSON.parse(this.cookieService.get('currentUser'));
    }*/

  }

  createUser(email: string, password: string, username: string) {
  }
  signUser(email: string, password: string) {
    return this.firebaseService.getFirebase().auth().signInWithEmailAndPassword(email, password);
  }

  signOutUser() {
    this.firebaseService.getFirebase().auth().signOut();
  }
}
