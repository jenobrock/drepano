import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Publication } from 'src/app/models';
import { PublicationRecentes } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  constructor(private httpClient: HttpClient) {}
  getPublication(){
   return this.httpClient.get<Publication[]>('https://us-central1-drepano-test.cloudfunctions.net/top_projet');
  }
  getPublicationRecent(){
    return this.httpClient.get<PublicationRecentes[]>('https://us-central1-drepano-test.cloudfunctions.net/get_projet_recent');
   }
}
